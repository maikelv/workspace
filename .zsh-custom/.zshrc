#!/bin/zsh

export WORKSPACE="${HOME}/workspace"

source ${WORKSPACE}/.zsh-custom/zshrc-oh-my-zsh
[[ -e ${WORKSPACE}/home/zsh-custom/zshrc-oh-my-zsh ]] && source ${WORKSPACE}/home/zsh-custom/zshrc-oh-my-zsh
source ${ZSH}/oh-my-zsh.sh

[[ -e ${WORKSPACE}/home/zsh-custom/zshrc-local ]] && source ${WORKSPACE}/home/zsh-custom/zshrc-local

source ${WORKSPACE}/.zsh-custom/zshrc-extra
[[ -e ${WORKSPACE}/home/zsh-custom/zshrc-extra ]] && source ${WORKSPACE}/home/zsh-custom/zshrc-extra



### MANAGED BY RANCHER DESKTOP START (DO NOT EDIT)
export PATH="/Users/maikel/.rd/bin:$PATH"
### MANAGED BY RANCHER DESKTOP END (DO NOT EDIT)
