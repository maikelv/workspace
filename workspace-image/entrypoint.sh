#!/bin/ash
set -e
[ $DEBUG ] && set -x

# set -o allexport
# source /home/${USER}/workspace_SETTINGS_FILE
# set +o allexport

# export WORKSPACE="${WORKSPACE:-${HOME}/workspace}"

# [ "/home/${USER}/workspace_SSH_KEY" == "" ] && echo 'Please set env WORKSPACE_SSH_KEY' && exit 1

if [ -f /usr/share/zoneinfo/$TIMEZONE ];then
    cp -f /usr/share/zoneinfo/$TIMEZONE /etc/localtime
else
    echo "Invalid timezone: $TIMEZONE"
    exit 1
fi


# # _ensure_workspace_git_folder

# if [ ! -e /home/${USER}/workspace/.git ] && [ -f /home/${USER}/workspace/upstream-workspace-repo.txt ];then
#     branch='master'
#     if [ -f /home/${USER}/workspace/upstream-workspace-branch.txt];then
#         branch=$(cat /home/${USER}/workspace/upstream-workspace-branch.txt)
#     fi
#     git clone \
#         --no-checkout \
#         --branch $branch \
#         $(cat /home/${USER}/workspace/upstream-workspace-repo.txt) \
#         /home/${USER}/workspace/upstream-workspace \
#     && mv /home/${USER}/workspace/upstream-workspace/.git /home/${USER}/workspace/.git \
#     && rmdir /home/${USER}/workspace/upstream-workspace
# fi


# if [ -f $HOME/.zshrc ] && [ ! -L $HOME/.zshrc ];then
#     cat $HOME/.zshrc >> /home/${USER}/workspace/zsh-custom/zshrc-legacy
#     echo '[ -e /home/${USER}/workspace/zsh-custom/zshrc-legacy ] && source /home/${USER}/workspace/zsh-custom/zshrc-legacy' >> /home/${USER}/workspace/zsh-custom/zshrc-local
# fi
# ln -sf /home/${USER}/workspace/.zsh/zshrc $HOME/.zshrc

# mkdir -p $HOME/.autoenv
# ln -sfn ${WORKSPACE/$HOME\//}/workspace-image/autoenv-activate.sh $HOME/.autoenv/activate.sh


# # _prepare_zsh

# if [ ! -d  /home/${USER}/workspace/.oh-my-zsh ];then
#     cp -r /var/lib/oh-my-zsh /home/${USER}/workspace/.oh-my-zsh
# fi

# echo "[ -f /home/${USER}/workspace/.zshrc ] && source /home/${USER}/workspace/.zshrc" > /etc/zsh/zshrc

# echo '' > /etc/zsh/zshenv
# env | while read env_var;do
#     if [ "${env_var:0:4}" == "PWD=" ];then
#         continue
#     fi
#     if [ "${env_var:0:9}" == "HOSTNAME=" ];then
#         env_var="HOSTNAME=\$(hostname)"
#     fi
#     echo "export $env_var" >> /etc/zsh/zshenv
# done
# echo "export KREW_ROOT=\"\${HOME}/workspace/home/.krew\"" >> /etc/zsh/zshenv
# echo 'export PATH="${KREW_ROOT}/bin:${PATH}"' >> /etc/zsh/zshenv
# echo '[ -e /home/${USER}/workspace/.zsh/zshenv ] && source /home/${USER}/workspace/.zsh/zshenv' >> /etc/zsh/zshenv


# Create user

WORKSPACE_PATH="/home/${USER}/workspace"

if [ ! -d "/home/${USER}" ];then
    cp -r /var/lib/home-workspace-template "/home/${USER}"
fi

mkdir -p "${WORKSPACE_PATH}"
if ! id -u "${USER}" >/dev/null 2>&1; then
    uid=$(ls ${WORKSPACE_PATH} -ld | awk '{print $3}')
    if [ "${uid}" == "root" ];then
        adduser -D -h /home/${USER} -s /bin/zsh -G staff ${USER}
    else
        adduser -D -h /home/${USER} -s /bin/zsh -G staff -u ${uid} ${USER}
    fi
    echo "${USER}:$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)" | chpasswd
    echo "${USER} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
fi

if [ ! -d /home/${USER}/.ssh ];then
    mkdir -p -m 700 /home/${USER}/.ssh
    chown ${USER} /home/${USER}/.ssh
fi

# cd /home/${USER}
# ssh_dir="$(dirname /home/${USER}/workspace_SSH_KEY)"
# if [ ! -d "$ssh_dir" ];then
#     mkdir -p -m 700 "$ssh_dir"
#     chown ${USER} "$ssh_dir"
# fi

# # Create a key to log in if not existing
# [ ! -f "/home/${USER}/workspace_SSH_KEY" ] && su -c "ssh-keygen -f '/home/${USER}/workspace_SSH_KEY' -N '' -t rsa -C '${USER}@workspace-host $(date)'" ${USER}

# # Add the ssh key to the authorized keys if not present
# grep -Fxq "$(cat /home/${USER}/workspace_SSH_KEY.pub)" /home/${USER}/.ssh/authorized_keys || cat /home/${USER}/workspace_SSH_KEY.pub >> /home/${USER}/.ssh/authorized_keys


/usr/sbin/sshd -p ${SSH_PORT:-22} -D
