[[ -f /etc/zsh/zshenv ]] && source /etc/zsh/zshenv

source ${HOME}/.zsh-custom/zshrc-oh-my-zsh
source ${HOME}/.zsh-custom/zshrc-extra
source ${HOME}/.zsh-custom/zshrc-local
source ${HOME}/.zsh-custom/zshrc-keybindings
